import React from "react";

import Header from "./Header";
import Footer from "./Footer";
import Portfolio from "./Portfolio";
import Info from "./Info";
import SiteLoader from "./SiteLoader";
import Error404 from "./Error404";

import ReactMotionTest from "../ReactMotionTest";

import store from '../store';

export default class MainLayout extends React.Component {
	
	constructor(props) {
	  super(props);
	  this.state = {
	  	preloaded : false
	  };
	}

	componentWillMount() {
		this.state.preloaded = store.getState().status.images_preloaded;
		console.log('is content preloaded: ' + this.state.preloaded);
	}

	

	componentWillUpdate(nextProps, nextState) {
		this.state.preloaded = true;
		console.log('is content preloaded: ' + this.state.preloaded);
	}


	componentDidUpdate(prevProps, prevState) {
		if(this.props.location.pathname == "/info"){
			document.body.classList.toggle('theme-info', true);
		}else{
			document.body.classList.toggle('theme-info', false);
		}
		
	}

	updatePreloadedState($update){
		console.log('onPreloadCompleteHandler called');
		this.setState( { preloaded : $update } );
	}

	render() {
		console.log('MainLayout Render')
		if(!this.state.preloaded){
			return(
				<SiteLoader onPreloadCompleteHandler={ this.updatePreloadedState.bind(this) } />
			);
		}else{
			return(
			    <section className="c-core">
			    	<div>
	                	<Header pathname={ this.props.location.pathname } />
	                	{React.cloneElement(this.props.children, this.props)}
	                	<Footer/>
	            	</div>
			    </section>
			);
		}

		
	}
}


/*
	<ReactMotionTest />
*/