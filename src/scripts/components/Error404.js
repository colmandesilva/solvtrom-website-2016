import React from "react";
import { browserHistory } from 'react-router';

export default class Error404 extends React.Component {
  
  onNavigateHome(){
    browserHistory.push("/");
  }

  render() {
    return (
      <section className="c-intro">
      	<div className="grid__row">
        	<div className="c-intro__greeting">404 Page not found</div>
        	<div className="c-intro__message">
        		<h1>Sorry. I could not find what you are looking for...</h1>

            <button onClick={this.onNavigateHome}> ------- Go home -------</button>
        	</div> 
        </div>    
      </section>
    );
  }
}