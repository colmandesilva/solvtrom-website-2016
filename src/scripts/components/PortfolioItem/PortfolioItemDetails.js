require('../../../styles/portfolioitem.scss');

import React from "react";

export default class PortfolioItemDetails extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {

    let url = "";
    if( this.props.url != undefined){
      url = "<a href='" + this.props.url + "' target='_blank'>Launch site</a>";
    }

    return (
      <div className="c-portfolioitem__details grid__row">
        <div className="grid__push-3 grid__col-3">
          <h4>Details</h4>
          <ul>
            { this.props.details.map(function(detail, i){
              return <li key={ i } dangerouslySetInnerHTML={{ __html : detail }}></li>
            }) }
          </ul>
        </div>

        <div className="grid__col-3">
          <h4>Role</h4>
          { this.props.role }
        </div>

        <div className="grid__col-3">
          <h4>Awards</h4>
          <ul>
            { this.props.awards.map(function(award, i){
              return <li key={ i } dangerouslySetInnerHTML={{ __html : award }}></li>
            }) }
          </ul>
        </div>

        <div className="c-portfolioitem__details-launchsite grid__col-4" dangerouslySetInnerHTML={{ __html : url }}>
        </div>


      </div>
    );
  }
}