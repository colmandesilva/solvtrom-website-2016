


export function loadData(){
	return {
		type : 'LOAD_DATA'
	}
}


export function initialPortfolioImages(images = []){
	return {
		type : 'PRELOAD_IMAGES',
		images
	}
}


export function showPortfolioItemDetail(index){
	return {
		type : 'PORTFOLIO_ITEM_SHOW',
		index
	}
}