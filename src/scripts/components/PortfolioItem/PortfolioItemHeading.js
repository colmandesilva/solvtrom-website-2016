require('../../../styles/portfolioitem.scss');


import React from "react";
import { Link } from 'react-router';

export default class PortfolioItemHeading extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="c-portfolioitem__heading grid__row">
        <div className="grid__col-3 c-portfolioitem__close">
          <Link to="/portfolio/">Close</Link>
        </div>
        <div className="grid__col-13">
          <div className="c-portfolioitem__heading-number">{ this.props.number } &mdash;</div>
          <div className="c-portfolioitem__heading-title">{ this.props.title }</div>
        </div>
      </div>
    );
  }
}



/*

<li className={this.state.classes} onClick={this.handleClick}>
        
        <div className="c-portfolio__heading grid__row">
          <div className="c-portfolioitem-close grid__col-3">
            X Close
          </div>  
          <div className="c-portfolioitem-title grid__col-13">
            <div className="c-portfolioitem-titlenumber">{ this.props.item.key } &mdash;</div>
            { this.props.item.title }
          </div>
        </div>
</li>
*/