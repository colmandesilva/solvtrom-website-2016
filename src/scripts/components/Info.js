require('../../styles/info.scss');

import React from "react";
import store from '../store';

export default class Info extends React.Component {
  render() {

    const about = store.getState().about;
    const skills = about.skills.split(',');
    const awards = about.awards.split(',');
    const clients = about.clients.split(',');
    const follows = about.follows;

    return (
      <div className="c-info">
      
        <div className="grid__row">
          <div className="c-info__profile">
            <div className="grid__col-3 grid__push-3">Profile</div>
            <div className="grid__col-8" dangerouslySetInnerHTML={{ __html : about.profile }} />
          </div>
        </div>

        <div className="grid__row">
          <div className="c-info__skills">
            <div className="grid__col-3 grid__push-3">Skills and specialities</div>
            <div className="grid__col-8">
              <ul>
                {skills.map(function(skill, i){
                  return <li key={i}> { skill } </li>
                })}
              </ul>
            </div>
          </div>
        </div>

        <div className="grid__row">
          <div className="c-info__awards">
            <div className="grid__col-3 grid__push-3">Awards & Recognition</div>
            <div className="grid__col-8">
              <ul>
                {awards.map(function(award, i){
                  return <li key={i}> { award } </li>
                })}
              </ul>
            </div>
          </div>
        </div>


        <div className="grid__row">
          <div className="c-info__clients">
            <div className="grid__col-3 grid__push-3">Clients</div>
            <div className="grid__col-8">
              <ul>
                {clients.map(function(client, i){
                  return <li key={i}> { client } </li>
                })}
              </ul>
            </div>
          </div>
        </div>

        <div className="grid__row">
          <div className="c-info__follow">
            <div className="grid__col-3 grid__push-3">Follow me on</div>
            <div className="grid__col-8">
              <ul>
                <ul>
                {follows.map(function(follow, i){
                  return <li key={i} dangerouslySetInnerHTML={{ __html : follow }} />
                })}
              </ul>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}