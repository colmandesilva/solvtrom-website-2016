import React from "react";

// Components
import Intro from "./Intro";
import PortfolioItem from "./PortfolioItem/PortfolioItem";
import { browserHistory } from 'react-router';

import store from '../store';

export default class Portfolio extends React.Component {
  
  constructor(props) {
    super(props);

    this.imageWidth = 2496;
    this.imageHeight = 1404;
    this.aspectraio = this.imageWidth / this.imageHeight;

    console.log(this.aspectraio);

    this.state = {
      activeChildId:this.matchPathName(this.props.location.pathname)[0],
      slug : this.props.location.pathname
    };



  }

  onClickHandler(item){
    // console.log('Portfolio -> onClickHandler()');
    browserHistory.push(item.slug);
  }

  shouldComponentUpdate(nextProps, nextState){
    // console.log('shouldComponentUpdate');
    if(nextProps.location.pathname != this.props.location.pathname) return true;
    return false;
  }

  componentWillMount() {
    // console.log('Portfolio -> componentWillMount()');
    // console.log('-------------------');
  }

  componentDidUpdate(prevProps, prevState) {
    
  }

  componentWillUpdate(nextProps, nextState) {
    // console.log('Portfolio -> componentWillUpdate() -> ');
    this.state.activeChildId = this.matchPathName(nextProps.location.pathname)[0];
    //this.setState({ activeChildId:this.matchPathName(nextProps.location.pathname)[0] });
    // console.log('-------------------');
  }


  matchPathName($matchTo){
    return(
      store.getState().portfolio.filter((item) => {
        return item.slug == $matchTo;
      })
    )
  }
  


  render() {
    const portfolio = store.getState().portfolio;
    const pathname = this.props.location.pathname;

    // console.log('Portfolio -> render()');
    // //console.log(this.state.activeChildId)
    // console.log('-------------------');

    return (
      <div>
        
        <Intro/>

        <section className="c-portfolio" id="c-portfolio">
          <div className="c-porfilio__header grid__row">
            <h2 className="grid__col-13">Selected work</h2>
          </div>
          
          <ul className="c-portfolioitem__list">
            {portfolio.map((item, i) => {
              return(
                <PortfolioItem
                  active={ this.state.activeChildId == item }
                  onClick={() => this.onClickHandler(item) }
                  key={i} i={i}
                  item={item} 
                />
              )
            })}
          </ul>
        </section> 

      </div>
    );
  }
}