import React from "react";
import { TransitionMotion, Motion, spring } from 'react-motion';
import ReactDOM from 'react-dom';


export default class ReactMotionTest extends React.Component {

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	items: [{key: 'a', size: 25, isOpen:false}, {key: 'b', size: 25, isOpen:false}, {key: 'c', size: 25, isOpen:false}],
	  };
	}


	handleClickMe(e){

		this.setState(
			{
				items : [
					{key: 'a', size: 25, isOpen:false},
					{key: 'b', size: 25, isOpen: Math.random() > 0.5 ? true : false},
					{key: 'c', size: 25, isOpen:false}
				]
			}
		);
	}

	

	render(){
		return(
			<div className="testWrapper" onClick={ this.handleClickMe.bind(this) }>
				{ 
					this.state.items.map( (item, i) => {
						

						// Dynamically update the "start" position of the animation?!

						if(item.isOpen){
							return(
								<Motion key={i} defaultStyle={{height: 25}} style={{height: spring(200)}} >
									{value => (
										<div style={{border:'1px solid', maxHeight:value.height}} className={ 'open' } >
											<div className="inner">
												Pellentesque ac ante feugiat, hendrerit elit nec, venenatis lectus. Fusce tincidunt non ligula vel facilisis. Nunc vitae pretium urna. Curabitur eget condimentum urna. Duis quis tempus justo. Sed convallis lorem in dui facilisis, et laoreet dolor consequat. Curabitur ornare risus imperdiet nulla dignissim, vel vehicula lectus consectetur. Mauris vehicula tempus placerat. Integer maximus finibus eros eget varius. Cras eget dui a nibh vestibulum finibus. Quisque elementum auctor porta. Praesent neque urna, placerat eu ex ac, blandit dignissim lectus. Quisque ut odio vel lectus sollicitudin tincidunt. Mauris a nisi velit. Morbi ut sapien sed nibh interdum placerat. Morbi odio eros, viverra nec ornare ut, scelerisque ac tortor.
											</div>
										</div>
									)}
								</Motion>
							)
						}else{
							return(
								<Motion key={i} defaultStyle={{height: 25}} style={{height: spring(item.size, {stiffness: 90, damping: 26})}} >
									{value => (
										<div style={{border:'1px solid', maxHeight:value.height}} className={ 'close' } >
											<div className="inner">
												Pellentesque ac ante feugiat, hendrerit elit nec, venenatis lectus. Fusce tincidunt non ligula vel facilisis. Nunc vitae pretium urna. Curabitur eget condimentum urna. Duis quis tempus justo. Sed convallis lorem in dui facilisis, et laoreet dolor consequat. Curabitur ornare risus imperdiet nulla dignissim, vel vehicula lectus consectetur. Mauris vehicula tempus placerat. Integer maximus finibus eros eget varius. Cras eget dui a nibh vestibulum finibus. Quisque elementum auctor porta. Praesent neque urna, placerat eu ex ac, blandit dignissim lectus. Quisque ut odio vel lectus sollicitudin tincidunt. Mauris a nisi velit. Morbi ut sapien sed nibh interdum placerat. Morbi odio eros, viverra nec ornare ut, scelerisque ac tortor.
											</div>
										</div>
									)}
								</Motion>
							)
						}

						
					})
				}			
			</div>
		);		
	}

}



// {value => <div style={{border:'1px solid', height:value.val}}>{Math.round(value.val)}</div>}