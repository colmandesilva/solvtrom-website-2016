
// import { applyMiddleware, createStore } from "redux";
// import axios from "axios";
// import logger from "redux-logger";
// import promise from "redux-promise-middleware";


// const initialState = {
//   fetching: false,
//   fetched: false,
//   data: null,
//   error: null,
// };


// const reducer = (state=initialState, action) => {
//   switch (action.type) {
//     case "FETCH_USERS_PENDING": {
//       return {...state, fetching: true}
//       break;
//     }
//     case "FETCH_USERS_REJECTED": {
//       return {...state, fetching: false, error: action.payload}
//       break;
//     }
//     case "FETCH_USERS_FULFILLED": {
//       return {
//         ...state,
//         fetching: false,
//         fetched: true,
//         data: action.payload,
//       }
//       break;
//     }
//   }
//   return state
// }

// const middleware = applyMiddleware(promise(), logger())
// const store = createStore(reducer, middleware)

// store.dispatch({
//   type: "FETCH_USERS",
//   payload: axios.get("data/portfolio.json")
// })



import { createStore, compose } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';

import rootReducer from './reducers/Index';

import { slugify } from './Utils';

// Load data (local) changes this to load dynamically once I get this up and running
import solvstrom from './data/solvstrom';

// Add SLUG
solvstrom.portfolio.map(function(item, i){
	solvstrom.portfolio[i].active = false;
	solvstrom.portfolio[i].slug = '/portfolio/' + slugify(solvstrom.portfolio[i].title);
});

const defaultState = {
	about : solvstrom.about,
	portfolio : solvstrom.portfolio,
	status : {
		data_preloaded : false,
		images_preloaded : false,
	}
}

const store = createStore(rootReducer, defaultState);

export const history = syncHistoryWithStore(browserHistory, store);
export default store



 