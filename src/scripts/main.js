require('../styles/main.scss');

import React from "react";
import ReactDOM from "react-dom";

import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

// Components
import Portfolio from "./components/Portfolio";
import Info from "./components/Info";
import Error404 from "./components/Error404";
import MainLayout from "./components/MainLayout";

import store, { history } from './store';


const router = (
	<Provider store={ store }>
		<Router history={ history }>
			<Route path="/" component={ MainLayout }>
				<IndexRoute component={ Portfolio } />
				<Route path="portfolio(/:slug)" component={ Portfolio } />
				<Route path="info" component={ Info } />
				<Route path="*" component={ Error404 } />
			</Route>
		</Router>
	</Provider>
)


const app = document.getElementById('app')

ReactDOM.render(router, app);
