require('../../../styles/portfolioitem.scss');

import React from "react";

export default class PortfolioItemIntro extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="c-portfolioitem__intro grid__row">
        <div className="grid__push-3 grid__col-6" dangerouslySetInnerHTML={{ __html : this.props.info }} />
      </div>
    );
  }
}