import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';


// Single reducers to combine
import status from './StatusReducer';
import portfolio from './PortfolioReducer';
import about from './AboutReducer';

const rootReducer = combineReducers({status, portfolio, about, routing:routerReducer});

export default rootReducer;