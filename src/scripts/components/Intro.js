import React from "react";

export default class Intro extends React.Component {
  render() {
    return (
      <section className="c-intro">
      	<div className="grid__row">
        	<div className="c-intro__greeting">Greetings</div>
        	<div className="c-intro__message">
        		<h1>Hello my friend.</h1>
        	</div>  
        </div>    
      </section>
    );
  }
}