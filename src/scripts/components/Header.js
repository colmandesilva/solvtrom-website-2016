import React from "react";
import { Link } from 'react-router';

import store from '../store';

export default class Header extends React.Component {


  isMenuActive($searchPath){
    var paths = this.props.pathname.split('/');
    if(paths[1] == $searchPath){
      return true;
    }else{
      return false;
    }

  }

  render() {

    const about = store.getState().about;

    return (
        <section className="c-header">
          <div className="grid__row">
            
            <div className="c-header__logo">
              <Link to={'/'} className="normal"><img src="/assets/common/logo.png" alt=""  /></Link>
              <Link to={'/'} className="inverse"><img src="/assets/common/logo-inverse.png" /></Link> 
            </div>


            <div className="c-header__about" dangerouslySetInnerHTML={{ __html : about.header_about }} />
            <div className="c-header__social" dangerouslySetInnerHTML={{ __html : about.header_social }} />

            <div className="c-header__nav">
              <ul>
                <li className={ this.isMenuActive('portfolio') ? "active" : "" }><Link to={'/portfolio'}>projects</Link></li>
                <li className={ this.isMenuActive('info') ? "active" : "" }><Link to={'/info'}>info</Link></li>
              </ul>
            </div>
    
          </div>
        </section>
    );
  }
}



/*

<div className="grid__row">
              <br/>
              <br/>
              <br/>
              <br/>
              <Link to={'/info'} activeClassName={"c-nav__active"}>info</Link>
              <br/>
              <Link to={'/portfolio'} activeClassName={"c-nav__active"}>Portfolio base</Link>
              <br/>
              <Link to={'/portfolio/10'} activeClassName={"c-nav__active"}>Portfolio->Case</Link>
              <br/>
              <Link to={'/This-is-some-random/page/handle/wildcard'} activeClassName={"c-nav__active"}>404 Error</Link>
            </div>

            */

