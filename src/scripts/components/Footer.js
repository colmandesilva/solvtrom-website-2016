import React from "react";


export default class Footer extends React.Component {
  render() {
    return (
      <footer className="c-footer">
      	<div className="grid__row">
          <div className="c-footer__copyright grid__col-8">
        		&copy; 2016<br/><b>Morten Solvstrom</b>
        	</div>
        	<div className="c-footer__contact grid__col-8">
        		email<br/><a href="mailto:hello@solvstrom.com">hello@solvstrom.com</a>
        	</div>
        </div>
      </footer>
    );
  }
}
