require('../../../styles/portfolioitem.scss');

import React from "react";
import ReactDOM from "react-dom";
import { Link } from 'react-router';

import PortfolioItemHeading from './PortfolioItemHeading';
import PortfolioItemIntro from './PortfolioItemIntro';
import Slideshow from './../SlideShow/Slideshow';
import PortfolioItemDetails from './PortfolioItemDetails';

require('smoothscroll-polyfill').polyfill();

import store from '../../store';

export default class PortfolioItem extends React.Component {
  
  constructor(props) {
    super(props);   
  }


  shouldComponentUpdate(nextProps, nextState){
    if(this.props.active != nextProps.active) return true;
    return false;
  }

  componentWillUpdate(){
    //console.log('PortfolioItem: ' + this.props.i + '-> componentWillUpdate()');
  }

  componentDidUpdate(prevProps, prevState) {
    
    if(this.props.active){
      var a = document.getElementById('c-portfolio');
      var offset = a.offsetTop + (this.props.i * 99) + 60;
      window.scroll({ top: offset, left: 0, behavior: 'smooth' });
      //ReactDOM.findDOMNode(this).scrollIntoView({ behavior: 'smooth' });
    }
  }


  render() {
    //console.log('PortfolioItem: ' + this.props.i + '-> render()');
    const item = this.props.item
    const url_name = item.slug;
    const slug = item.slug;

    let classes = this.props.active ? "c-portfolioitem show" : "c-portfolioitem"; 

    return (
        <li className={ classes } onClick = { this.props.active ? null : this.props.onClick }>
          <PortfolioItemHeading number={item.key} title={item.title} />
          
          <div className="c-portfolioitem__inner">
            <PortfolioItemIntro info={item.info} />
            <Slideshow images={item.images} active={ this.props.active } />
            <PortfolioItemDetails details={item.details} role={item.role} awards={item.awards} url={item.url} />
          </div>
        </li>
    );
  }
}