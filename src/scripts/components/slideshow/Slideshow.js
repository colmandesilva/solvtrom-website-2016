import React from "react";

require('../../../styles/slideshow.scss');

export default class Slideshow extends React.Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
      imagesLoaded : false
    };
  }

  componentDidMount() {
    if(this.props.active && !this.state.imagesLoaded){
      this.preloadImages();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.active && !this.state.imagesLoaded){
      this.preloadImages();
    }
  }

  preloadImages(){
    console.log('Preload my images please :)')
    console.log(this.props.images[0], this.props.active)
    
    setTimeout(function(){
      this.setState({ imagesLoaded : true });
    }.bind(this), 100);
    
  }


  render() {

    // temporary on render placeholder image
    // return (
    //     <div className="c-portfolioitem__slideshow grid__row show">
    //       <div id="slider" className="grid__row">
    //         <figure className="grid__col-16">
    //           <img src="/assets/portfolio/placeholder.jpg" alt=""/>
    //         </figure>
    //       </div>
    //     </div>
    //   );
    
    if(this.state.imagesLoaded){
      return (
        <div className="c-portfolioitem__slideshow grid__row show">
          <div id="slider" className="grid__row">
            <figure className="grid__col-16">
              { this.props.images.map(function(image, i){
                return (<img src={ image } alt key={i} />)
              }) }
            </figure>
          </div>
        </div>
      );
    }else{
      return (
        <div className="c-portfolioitem__slideshow grid__row">
          <div id="slider" className="grid__row">
            <div className="grid__col-13 grid__push-3">
              <img src="/assets/common/loader.gif" alt=""/>
            </div>
          </div>
        </div>
      );

    }

  }
}