import React from "react";
import axios from "axios";
import { browserHistory } from 'react-router';

import store from '../store';

export default class SiteLoader extends React.Component {
  
  constructor(props) {
    super(props); 

    let portfolio, imagesArray, imagesLoaded, imagesTotal;
  }

  componentWillMount() {

    let imagesArray, imagesLoaded, imagesTotal;
    this.portfolio = store.getState().portfolio;

    this.imagesArray = [];

    for(var i=0;i< this.portfolio.length;i++){
      var imagesLength = this.portfolio[i].images.length;
      for(var j=0;j<imagesLength;j++){
        this.imagesArray.push(this.portfolio[i].images[j]);
      }
    }

    this.imagesLoaded = 0;
    this.imagesTotal = this.imagesArray.length;
  }

  componentDidMount() {
    this.preload(this.imagesArray);
  }


  preload(imageArray) {
     for (var i = 0; i < imageArray.length; ++i) {
         var img = new Image();
         img.onload = this.onLoadComplete.bind(this);
         img.src = imageArray[i];
      }
  }

  onLoadComplete(){
    this.imagesLoaded++;
    if(this.imagesLoaded == this.imagesTotal){
      console.log('load complete, show website');
      
      store.getState().status.images_preloaded = true; 
      this.props.onPreloadCompleteHandler(true);
    }
  }


  render() {
    return (
      <img src="/assets/common/loader.gif" alt=""/>
    );
  }
}