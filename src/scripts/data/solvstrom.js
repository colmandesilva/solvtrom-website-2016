const solvstrom = {

	"about" : {
		"header_about" : "Hi, i’m <b>Morten Solvstrom</b><br/>I’m a Danish designer and Art Director currently<br/>working at Hello Monday in New York",
		"header_social" : "I take photos, so make sure to check out my<br/><a href=''>Instagram</a>. I also use <a href=''>Dribbble</a> once in a while,<br/>and I also have <a href=''>Twitter</a> account i never use.",

		"profile" : "<h2>I’m a Senior Art Director based in New York. I’m a designer by heart and always looking for a good story to tell.I have 10 years of experience and a broad set of skills in Concept Development & Workshops, Strategy, UX / UI, Art Direction, Photography & Video.</h2><p>I love to work on projects that have an impact and that i truly believe in. We live in a world where a lot of bad stuff is happening, so being able to help just a little bit makes all the difference. It is important for me to work with likeminded people who share the same values as i do.</p><div>",
		
		"skills" : "Concept Development,Art Direction,Interactive Experiences,UX / UI,Graphic Design,BrandingVideo,Photography",
		"awards" : "Webby Awards, Cannes Young Cyber Lion Finalists,Creative Circle Awards,Webby Awards,FWA - Site of the Day,FWA - Mobile of the Day,Awwwards - Site of the Day,Awwwards - Site of the Month,CSS Webawards,Siteinspire,Photoshop Advanced feature,.NET magazine feature,Computer Arts - Inside artist",
		"clients" : "National Geographic,Google,YouTube,Samsung,Reebok,ECCO,Microsoft,Red Bull,Vestas,MTV,Konica",
		"follows" : [
			"<a href='#' target='_blank'>Instagram</a>",
			"<a href='#' target='_blank'>Twitter</a>",
			"<a href='#' target='_blank'>LinkedIn</a>"
		]
	},
	

	"portfolio" : [
		{
			"key" : '01',
			"backgroundColor" : "#e2e2e2",

			"title" : "Polyera - Wave™ Band",
			"type" : "App 1",
			"info" : "<p>Polyera is twisting the concept of wearable technology into something that is always on, fully flexible, and completely customizable.</p><p>After 10 years of developing the technology, they’re finally ready to unleash their product Wove onto the world.</p>",
			"details" : [
			 "Client: Polyera",
			 "Year: 2015",
			 "Agency: <a href='http://www.hellomonday.com' target='_blank'>Hello Monday</a>"
			],
			"role" : [
				"Concept + Creative Direction + Art Direction + Design"
			],
			"awards" : [
				"FWA, Site of the Day",
				"FWA, Mobile of the Day",
				"Awwwards, Site of the Day",
				"Webby Awards 2016 - Consumer electronics"
			],
			"images" : [
				"/assets/portfolio/Polyera-01.jpg",
				"/assets/portfolio/Polyera-02.jpg",
				"/assets/portfolio/Polyera-03.jpg",
				"/assets/portfolio/Polyera-04.jpg",
				"/assets/portfolio/Polyera-04.jpg"
			],

			"url" : "http://www.google.com"
		},
		{
			"key" : '02',
			"backgroundColor" : "#e2e2e2",

			"title" : "Revelator",
			"type" : "App 2",
			"info" : "<p>Polyera is twisting the concept of wearable technology into something that is always on, fully flexible, and completely customizable.</p><p>After 10 years of developing the technology, they’re finally ready to unleash their product Wove onto the world.</p>",
			
			"details" : [
			 "Client: Revelator",
			 "Year: 2015",
			 "Agency: <a href='http://www.hellomonday.com' target='_blank'>Hello Monday</a>"
			],
			"role" : [
				"Concept + Creative Direction + Art Direction + Design"
			],
			"awards" : [
				"FWA, Site of the Day",
				"FWA, Mobile of the Day",
				"Awwwards, Site of the Day",
				"Webby Awards 2016 - Consumer electronics"
			],

			"images" : [
				"/assets/portfolio/Revelator-01.jpg",
				"/assets/portfolio/Revelator-02.jpg",
				"/assets/portfolio/Revelator-03.jpg",
				"/assets/portfolio/Revelator-04.jpg",
				"/assets/portfolio/Revelator-04.jpg"
			],
		},
		{
			"key" : '03',
			"backgroundColor" : "#e2e2e2",

			"title" : "Yellowstone Bears",
			"type" : "App 3",
			"info" : "<p>Polyera is twisting the concept of wearable technology into something that is always on, fully flexible, and completely customizable.</p><p>After 10 years of developing the technology, they’re finally ready to unleash their product Wove onto the world.</p>",
			
			"details" : [
			 "Client: National Geographic",
			 "Year: 2015",
			 "Agency: <a href='http://www.hellomonday.com' target='_blank'>Hello Monday</a>"
			],
			"role" : [
				"Concept + Creative Direction + Art Direction + Design"
			],
			"awards" : [
				"FWA, Site of the Day",
				"FWA, Mobile of the Day",
				"Awwwards, Site of the Day",
				"Webby Awards 2016 - Consumer electronics"
			],
			"images" : [
				"/assets/portfolio/Bears-01.jpg",
				"/assets/portfolio/Bears-02.jpg",
				"/assets/portfolio/Bears-03.jpg",
				"/assets/portfolio/Bears-04.jpg",
				"/assets/portfolio/Bears-04.jpg"
			],
		}
	]
}

export default solvstrom;